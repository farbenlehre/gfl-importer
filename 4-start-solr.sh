#!/bin/bash

if [ ! -e docker.env ]; then
	touch docker.env
fi
chmod a+w solr/gfl solr/gfl/core.properties solr/gfloffline solr/gfloffline/core.properties
docker-compose up -d solr
