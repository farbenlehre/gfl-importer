# Importer plugin for "Goethes Farbenlehre"

## New way with Go

just clone the projekt and run 

```./4-start-solr.sh```

or

```docker-compose up -d solr```

## System requirements

Docker, docker-compose, Git.

## Installation of the importer

This project is only a plugin for the actual importer. That's why you need to get the code of the solr-importer by executing:

```./1-clone-solr-importer.sh```

Make sure you understand the concepts behind the implementation of the importer by reading the documentation here:

https://github.com/subugoe/solr-importer

## Starting the importer

For now, there is no docker container to execute the importer. If you want to extend this project in that way, you can look how it's done here:

https://github.com/subugoe/fwb-importer

The easiest way to execute the importer on your local system is from inside Eclipse. The procedure is described here:

https://github.com/subugoe/solr-importer#starting-in-eclipse

Secondly, it is possible to compile and start it manually. The compilation is Docker-based:

```./2-compile.sh```

To start the importer, you must set some environment variables and execute the .jar file, as described here (ignore the 'gradle' commands):

https://github.com/subugoe/solr-importer#installation-as-a-stand-alone-tool

## Installing and starting a Solr server

The solr/ directory contains all the files that are necessary to start a Solr server. The server's configuration and schema will be compatible with the Solr XML files produced by the importer. You can use the solr/ directory to configure your own Solr server.

However, this project also offers an out-of-the-box solution based on Docker. In a development environment, you can use the same Git clone both for the importer and for a Solr server. In a production environment, you should just clone this whole project into its own directory and use it only for Solr.

First, you can configure the port on which Solr will start. The default port is 8983. To change it to e. g. 8984, edit the file docker-compose.yml:

```
  solr:
    ...  
    ports:
      - 8984:8983      
```

Now you can start Solr:

```./4-start-solr.sh```

Note: If you want to install several Solr servers on the same host, you need to watch out for a little thing. The tool 'docker-compose' uses the parent directory name to identify Docker images. You can easily clone this project one more time and start a second Solr server. However, the parent directory must be different. The easiest way to achieve this is to clone to a directory with a different name:

```git clone <this project> other-directory/```

Also, it is often required to maintain several Solr servers for different environments, like 'dev' or 'live'. In that case, you can work with different Git branches in different clones.

## Updating Solr server

You can update all Solr configuration which has been changed in the remote Git repository by executing:

```
./6-update-sources.sh
./5-restart-solr.sh
```
