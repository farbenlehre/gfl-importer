package sub.gfl.api;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import sub.ent.api.ImporterStep;
import sub.ent.backend.FileAccess;
import sub.ent.backend.Xslt;

public class ImporterStepConvert extends ImporterStep {

	private Xslt xslt = new Xslt();
	private FileAccess fileAccess = new FileAccess();

	@Override
	public void execute(Map<String, String> params) throws Exception {
		String gitDir = params.get("gitDir");
		String solrXmlDir = params.get("solrXmlDir");
		File outputDir = new File(solrXmlDir);
		File inputDir = new File(gitDir);

		fileAccess.cleanDir(outputDir);
		out.println("    Converting TEIs to index files:");

		InputStream xsltStream = ImporterStepConvert.class.getResourceAsStream("/gfl-indexer.xslt");
		xslt.setXsltScript(xsltStream);
		xslt.setErrorOut(out);

		List<File> allFiles = fileAccess.getAllXmlFilesFromDir(inputDir);
		Collections.sort(allFiles);
		
		int counter = 1;
		for (File currentFile : allFiles) {
			printCurrentStatus(counter, allFiles.size());
			OutputStream fileOs = fileAccess.createOutputStream(new File(solrXmlDir), currentFile.getName());
			xslt.transform(currentFile.getAbsolutePath(), fileOs);
			counter++;
		}
		
	}
	
	private void printCurrentStatus(int currentNumber, int lastNumber) {
		if (currentNumber % 1000 == 0 || currentNumber == lastNumber) {
			out.println("    ... " + currentNumber);
		}
	}

	@Override
	public String getStepDescription() {
		return "Konvertierung TEI -> XML";
	}

}
