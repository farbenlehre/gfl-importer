package sub.gfl;

import static org.junit.Assert.*;
import static org.custommonkey.xmlunit.XMLAssert.*;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sub.ent.backend.Xslt;

public class XsltHtmlTest {

	private OutputStream outputBaos;
	private static Xslt xslt;

	@BeforeClass
	public static void beforeAllTests() throws Exception {
		xslt = new Xslt();
		xslt.setXsltScript("src/main/resources/gfl-indexer.xslt");
	}

	@Before
	public void beforeEachTest() throws Exception {
		outputBaos = new ByteArrayOutputStream();
	}

	@After
	public void afterEachTest() {
		 System.out.println(outputBaos.toString());
	}

	@Test
	public void pageBreak_findsCorrespondingUrlInFacsimile() throws Exception {
		String html = transform("page-break_with-graphic-url.xml");

		assertXpathEvaluatesTo("/my-graphic", "//div[@class='page-beginning']/a/@href", html);
	}
	
	@Test
	public void postscript() throws Exception {
		String html = transform("postscript-with-p.xml");

		assertXpathEvaluatesTo("PS", "//div[@class='postscript']/div[@class='p']", html);
	}
	
	@Test
	public void choice() throws Exception {
		String html = transform("choice-with-abbreviation.xml");

		assertXpathEvaluatesTo("d.", "//div[@class='abbr']", html);
		assertXpathEvaluatesTo("den", "//div[@class='expan']", html);
	}
	
	@Test
	public void label() throws Exception {
		String html = transform("label.xml");

		assertXpathEvaluatesTo("my-label", "//div[@class='label']", html);
	}
	
	@Test
	public void label_withRendition() throws Exception {
		String html = transform("label_with-rendition.xml");

		assertXpathEvaluatesTo("label in the center", "//div[@class='label centre']", html);
	}
	
	@Test
	public void rsWithType() throws Exception {
		String html = transform("rs-place-and-person.xml");

		assertXpathEvaluatesTo("My place", "//div[@class='rs-place']", html);
		assertXpathEvaluatesTo("My person", "//div[@class='rs-person']", html);
	}
	
	@Test
	public void headRenditions() throws Exception {
		String html = transform("head_with-renditions.xml");

		assertXpathEvaluatesTo("column to the right", "//div[@class='head column-right']", html);
		assertXpathEvaluatesTo("in the center", "//div[@class='head centre']", html);
		assertXpathEvaluatesTo("underlined head", "//div[@class='head underline']", html);
	}
	
	@Test
	public void softHyphen_convertsToMinus() throws Exception {
		String html = transform("invisible-softhyphen-0173.xml");

		assertXpathEvaluatesTo(" auf- und ab", "//div[@class='article']", html);
	}
	
	@Test
	public void datelineWithDate() throws Exception {
		String html = transform("dateline-with-date.xml");

		assertXpathEvaluatesTo("before 1821 after", "//div[@class='closer']/div[@class='dateline']", html);
		assertXpathEvaluatesTo("1821", "//div[@class='closer']//div[@class='date']", html);
	}
	
	@Test
	public void signed() throws Exception {
		String html = transform("signed.xml");

		assertXpathEvaluatesTo("Me", "//div[@class='closer']/div[@class='signed']", html);
	}
	
	@Test
	public void closer() throws Exception {
		String html = transform("closer-and-salute.xml");

		assertXpathEvaluatesTo("mfg", "//div[@class='closer']/div[@class='salute']", html);
	}
	
	@Test
	public void unknownElement() throws Exception {
		String html = transform("unknown-element.xml");

		assertXpathEvaluatesTo("new stuff", "//span[@class='unknown-element']", html);
	}
	
	@Test
	public void object() throws Exception {
		String html = transform("object.xml");

		assertXpathEvaluatesTo("my object", "//div[@class='object']", html);
	}
	
	@Test
	public void biblRef_makesAhref() throws Exception {
		String html = transform("bibl-ref.xml");

		assertXpathExists("//div[@class='bibl']", html);
		assertXpathEvaluatesTo("my link", "//a", html);
	}
	
//	@Test
//	public void hiRenditions() throws Exception {
//		String html = transform("hi-renditions.xml");
//
//		assertXpathEvaluatesTo("underlined", "//div[@class='underline']", html);
//		assertXpathEvaluatesTo("superscripted", "//sup", html);
//		assertXpathEvaluatesTo("subscripted", "//sub", html);
//		assertXpathEvaluatesTo("italicized", "//div[@class='italic']", html);
//		assertXpathEvaluatesTo("double-underlined", "//div[@class='doubleunderline']", html);
//		assertXpathEvaluatesTo("letterspaced", "//div[@class='letterspace']", html);
//		assertXpathEvaluatesTo("right", "//div[@class='right']", html);
//	}
	
	@Test
	public void noteFootnote() throws Exception {
		String html = transform("note-footnote.xml");

		assertXpathEvaluatesTo("A note.", "//div[@class='note-footnote']", html);
	}
	
	@Test
	public void noteEnd() throws Exception {
		String html = transform("note-without-type.xml");

		assertXpathEvaluatesTo("A note.", "//div[@class='note']", html);
	}
	
	@Test
	public void noteComment() throws Exception {
		String html = transform("note-comment.xml");

		assertXpathEvaluatesTo("A note.", "//div[@class='note-comment']", html);
	}
	
	@Test
	public void segWithRs() throws Exception {
		String html = transform("seg-with-rs-person.xml");

		assertXpathEvaluatesTo("hiesigen Mechanikers", "//div[@class='seg']", html);
		assertXpathEvaluatesTo("Mechanikers", "//div[@class='rs-person']", html);
	}
	
	@Test
	public void person() throws Exception {
		String html = transform("person.xml");

		assertXpathEvaluatesTo("My person", "//div[@class='person']", html);
	}
	
	@Test
	public void org() throws Exception {
		String html = transform("organization.xml");

		assertXpathEvaluatesTo("My org", "//div[@class='org']", html);
	}
		
	@Test
	public void pageBreak() throws Exception {
		String html = transform("page-break.xml");

		assertXpathEvaluatesTo("2", "count(//div[@class='page'])", html);
	}
	
	@Test
	public void namePlace() throws Exception {
		String html = transform("name-place.xml");

		assertXpathEvaluatesTo("Jena", "//div[@class='place']", html);
	}
	
	@Test
	public void paragraphs() throws Exception {
		String html = transform("paragraph.xml");

		assertXpathEvaluatesTo("my p1", "//div[@class='p'][1]", html);
		assertXpathEvaluatesTo("my p2", "//div[@class='p'][2]", html);
	}
	
	@Test
	public void zeroPointSixLines_generateOneBreak() throws Exception {
		String html = transform("spaceLines_zeroPointSixLines.xml");

		assertXpathEvaluatesTo("1", "count(/div[@class='article']//br)", html);
	}
	
	@Test
	public void spaceLines_generateBreaks() throws Exception {
		String html = transform("spaceLines.xml");

		assertXpathEvaluatesTo("2", "count(/div[@class='article']//br)", html);
	}
	
	@Test
	public void opener_withSaluteAndLinebreak() throws Exception {
		String html = transform("opener-and-salute.xml");

		assertXpathExists("//div[@class='opener']/div[@class='salute']/br", html);
		assertXpathEvaluatesTo("Hochwohlgebohrener Herrund Staatsminister,", "//div[@class='salute']", html);
	}
	
	private String transform(String fileName) throws Exception {
		xslt.transform("src/test/resources/tei-snippets/" + fileName, outputBaos);
		return extractHtmlField(outputBaos.toString());
	}

	private String extractHtmlField(String s) {
		Pattern pattern = Pattern.compile("fulltext_html\"><!\\[CDATA\\[(.*?)]]");
		Matcher matcher = pattern.matcher(s.replaceAll("\\n", " "));
		String html = "";
		if (matcher.find()) {
			html = matcher.group(1);
		}
		return html;
	}

}
