package sub.gfl;

import static org.custommonkey.xmlunit.XMLAssert.assertXpathEvaluatesTo;
import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sub.ent.backend.Xslt;

public class XsltSplitTest {

	private OutputStream outputBaos;
	private static Xslt xslt;

	@BeforeClass
	public static void beforeAllTests() throws Exception {
		xslt = new Xslt();
		xslt.setXsltScript("src/main/resources/gfl-indexer.xslt");
	}

	@Before
	public void beforeEachTest() throws Exception {
		outputBaos = new ByteArrayOutputStream();
	}

	@After
	public void afterEachTest() {
		 System.out.println(outputBaos.toString());
	}

	@Test
	public void twoPages() throws Exception {
		String htmlPage1 = transform("two-page-beginnings.xml", 1);
		assertXpathEvaluatesTo("Page 1", "//div[@class='p'][1]", htmlPage1);

		String htmlPage2 = transform("two-page-beginnings.xml", 2);
		assertXpathEvaluatesTo("Second page", "//div[@class='p'][1]", htmlPage2);
	}
	
	@Test
	public void givesNumberOfPages() throws Exception {
		xslt.transform("src/test/resources/tei-snippets-split/" + "two-page-beginnings.xml", outputBaos);
		String xml = outputBaos.toString();
		assertXpathEvaluatesTo("2", "//field[@name='number_of_pages']", xml);
		assertXpathEvaluatesTo("my_id_page1", "//doc[2]/field[@name='id']", xml);
	}

	
	private String transform(String fileName, int pageNumber) throws Exception {
		xslt.transform("src/test/resources/tei-snippets-split/" + fileName, outputBaos);
		return extractHtmlField(outputBaos.toString(), pageNumber);
	}

	private String extractHtmlField(String s, int pageNumber) {
		Pattern pattern = Pattern.compile("html_page\"><!\\[CDATA\\[(.*?)]]");
		Matcher matcher = pattern.matcher(s.replaceAll("\\n", " "));
		String html = "";
		for (int i = 0; i < pageNumber; i++) {
			matcher.find();
		}
		html = matcher.group(1);
		return html;
	}

}
