package sub.gfl;

import static org.junit.Assert.*;
import static org.custommonkey.xmlunit.XMLAssert.*;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import net.sf.saxon.s9api.SaxonApiException;
import sub.ent.backend.Xslt;

public class XsltTest {

	private OutputStream outputBaos;
	private static Xslt xslt;

	@BeforeClass
	public static void beforeAllTests() throws Exception {
		xslt = new Xslt();
		xslt.setXsltScript("src/main/resources/gfl-indexer.xslt");
	}

	@Before
	public void beforeEachTest() throws Exception {
		outputBaos = new ByteArrayOutputStream();
	}

	@After
	public void afterEachTest() {
		 System.out.println(outputBaos.toString());
	}

	@Test
	public void license() throws Exception {
		String result = transform("licence.xml");

		assertXpathEvaluatesTo("CC BY-NC-SA-4.0", "//field[@name='license']", result);
	}

	@Test
	public void twoDifferentLanguages() throws Exception {
		String result = transform("language_twoDifferentEntries.xml");

		assertXpathEvaluatesTo("2", "count(//field[@name='language'])", result);
		assertXpathEvaluatesTo("ger", "//field[@name='language'][1]", result);
		assertXpathEvaluatesTo("eng", "//field[@name='language'][2]", result);
	}

	@Test
	public void languageOnPage() throws Exception {
		String result = transform("language.xml");

		assertXpathEvaluatesTo("ger", "//field[text()='page']/../field[@name='language']", result);
	}

	@Test
	public void oneLanguage() throws Exception {
		String result = transform("language.xml");

		assertXpathEvaluatesTo("ger", "//field[@name='language']", result);
	}

//	@Test
//	public void spaceAfterAddressLine() throws Exception {
//		String result = transform("address-in-opener.xml");
//
//		assertXpathEvaluatesTo("An des ", "//field[@name='fulltext']", result);
//	}

	@Test
	public void spaceAfterSaluteAndSigned() throws Exception {
		String result = transform("closer-with-salute-and-signed.xml");

		assertXpathEvaluatesTo("treulichst Goethe. ", "//field[@name='fulltext']", result);
	}

	@Test
	public void differentHyphensAtLinebreak() throws Exception {
		String result = transform("hyphens.xml");

		assertXpathEvaluatesTo("Worttrennung soft hyphen: - New sentence. Anna-Lena ", "//field[@name='fulltext']", result);
	}

	@Test
	public void pageBreak_makesSpace() throws Exception {
		String result = transform("page-break.xml");

		assertXpathEvaluatesTo(" before after ", "//field[@name='fulltext']", result);
	}

	@Test
	public void spaceLines_addsSpaceInText() throws Exception {
		String result = transform("spaceLines_withText.xml");

		assertXpathEvaluatesTo("myopener myparagraph ", "//field[@name='fulltext']", result);
	}

	@Test
	public void freeKeywords() throws Exception {
		String result = transform("keywords_free.xml");

		assertXpathEvaluatesTo("Handschrift", "//field[@name='free_keyword'][1]", result);
		assertXpathEvaluatesTo("Apparat", "//field[@name='free_keyword'][2]", result);
	}

	@Test
	public void gndKeywords() throws Exception {
		String result = transform("keywords_gnd.xml");

		assertXpathEvaluatesTo("Brief", "//field[@name='gnd_keyword'][1]", result);
		assertXpathEvaluatesTo("Goethe", "//field[@name='gnd_keyword'][2]", result);
	}

	@Test
	public void sourceDescription() throws Exception {
		String result = transform("sourceDesc.xml");

		assertXpathEvaluatesTo(" DE Weimar ", "//field[@name='source_description']", result);
	}

	@Test
	public void date() throws Exception {
		String result = transform("date.xml");

		assertXpathEvaluatesTo("1822-01-19", "//field[@name='origin_date']", result);
	}

	@Test
	public void recipient() throws Exception {
		String result = transform("recipient.xml");

		assertXpathEvaluatesTo("Dude", "//field[@name='recipient']", result);
	}

	@Test
	public void destinationPlace() throws Exception {
		String result = transform("place_destination.xml");

		assertXpathEvaluatesTo("Weimar", "//field[@name='destination_place']", result);
	}

	@Test
	public void originPlace() throws Exception {
		String result = transform("place_origin.xml");

		assertXpathEvaluatesTo("Berlin", "//field[@name='origin_place']", result);
	}

	@Test
	public void author() throws Exception {
		String result = transform("author.xml");

		assertXpathEvaluatesTo("Some author", "//field[@name='author']", result);
	}

	@Test
	public void fullTitleWithSpaces_replacesWithOneSpace() throws Exception {
		String result = transform("title_full_with_many_spaces.xml");

		assertXpathEvaluatesTo("My title, with many spaces", "//field[@name='title']", result);
	}

	@Test
	public void fullTitleWithPlace_createsField() throws Exception {
		String result = transform("title_full_with_place.xml");

		assertXpathEvaluatesTo("My Berlin title", "//field[@name='title']", result);
	}

	@Test
	public void fullTitle_createsField() throws Exception {
		String result = transform("title_full.xml");

		assertXpathEvaluatesTo("My full title", "//field[@name='title']", result);
	}

	@Test
	public void shortTitle_createsField() throws Exception {
		String result = transform("title_short.xml");

		assertXpathEvaluatesTo("My short title", "//field[@name='short_title']", result);
	}

	@Test
	public void note_makesExtraField() throws Exception {
		String result = transform("note-comment.xml");

		assertXpathEvaluatesTo("A note.", "//field[@name='note_comment']", result);
	}

	@Test
	public void noteWithRef_makesExtraField() throws Exception {
		String result = transform("note-comment_with-ref.xml");

		assertXpathEvaluatesTo("A note in ref.", "//field[@name='note_comment']", result);
	}

	@Test
	public void note_removesIfIsAComment() throws Exception {
		String result = transform("note-comment.xml");

		assertXpathEvaluatesTo(" before-note after-note ", "//field[@name='fulltext']", result);
	}

	@Test
	public void twoLinebreaks_replacesWithSpaces() throws Exception {
		String result = transform("linebreaks-two.xml");

		assertXpathEvaluatesTo("before middle after ", "//field[@name='fulltext']", result);
	}

	@Test
	public void linebreak_replacesWithSpace() throws Exception {
		String result = transform("linebreak.xml");

		assertXpathEvaluatesTo("before after ", "//field[@name='fulltext']", result);
	}

	@Test
	public void basicTest_fulltext() throws Exception {
		String result = transform("fulltext.xml");

		assertXpathEvaluatesTo("Test text. ", "//field[@name='fulltext']", result);
	}

	private String transform(String fileName) throws SaxonApiException {
		xslt.transform("src/test/resources/tei-snippets/" + fileName, outputBaos);
		return outputBaos.toString();
	}

}
